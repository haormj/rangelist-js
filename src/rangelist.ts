export class RangeList {
    rs: Range[]

    constructor() {
        this.rs = []
    }

    /**
     * Adds a range to the list
     * @param {Array<number>} range - Array of two integers that specify beginning and end of range.
     */
    add(ns: number[]) {
        var r = new Range(ns[0], ns[ns.length - 1])
        var tempRs: Range[] = [r]
        this.rs.forEach((value) => {
            tempRs = tempRs.concat(tempRs.pop()!.union(value))
        })
        this.rs = tempRs
    }

    /**
     * Removes a range from the list
     * @param {Array<number>} range - Array of two integers that specify beginning and end of range.
     */
    remove(ns: number[]) {
        var r = new Range(ns[0], ns[ns.length - 1])
        var tempRs: Range[] = []
        this.rs.forEach((value) => {
            tempRs = tempRs.concat(value.difference(r))
        })
        this.rs = tempRs
    }

    /**
     * Prints out the list of ranges in the range list
     */
    print() {
        console.log(this.toString())
    }

    /**
     * Returns RangeList string
     */
    toString(): string {
        return this.rs.join(' ')
    }
}

class Range {
    start: number
    end: number

    constructor(start: number, end: number) {
        this.start = start
        this.end = end
    }

    /**
     * Returns the union of two range, this ∪ r
     * @param {Range} r - record the range start and end
     */
    public union(r: Range): Range[] {
        if (this.start <= r.start) {
            return this.internalUnion(this, r)
        }
        return this.internalUnion(r, this)
    }

    /**
     * Interval method, union two range, must r1.start <= r2.start
     */
    private internalUnion(r1: Range, r2: Range): Range[] {
        if (r2.start >= r1.start && r2.start <= r1.end) {
            return [new Range(r1.start, Math.max(r1.end, r2.end))]
        }
        return [r1, r2]
    }

    /**
     * Returns the difference of this and r, this - r
     * @param {Range} r - record the range start and end
     */
    public difference(r: Range): Range[] {
        if (r.start <= this.start && r.end >= this.end) {
            return []
        }
        if (r.start >= this.start && r.start <= this.end) {
            var result: Range[] = []
            if (this.start < r.start) {
                result.push(new Range(this.start, r.start))
            }
            if (r.end < this.end) {
                result.push(new Range(r.end, this.end))
            }
            return result
        }
        if (r.end >= this.start && r.end <= this.end) {
            if (r.end < this.end) {
                return [new Range(r.end, this.end)]
            }
        }
        return [this]
    }

    /**
     * Returns Range to string
     */
    toString(): string {
        return '[' + this.start + ', ' + this.end + ')'
    }
}
