## rangelist-js

### Getting started

- clone
```
git clone git@gitlab.com:haormj/rangelist-js.git
```

- install dependencies
```
npm install
```

- test suites
```
npm run test
```

- test result
```
> rangelist@1.0.0 test
> jest --config jestconfig.json

  console.log
    [1, 5)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 5) [10, 20)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 5) [10, 21)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 5) [10, 21)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 8) [10, 21)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 8) [10, 21)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 8) [11, 21)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 8) [11, 15) [17, 21)

      at RangeList.print (src/rangelist.ts:38:17)

  console.log
    [1, 3) [19, 21)

      at RangeList.print (src/rangelist.ts:38:17)

 PASS  test/rangelist.test.ts
  ✓ RangeList test suites (30 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.44 s, estimated 2 s
Ran all test suites.
```
